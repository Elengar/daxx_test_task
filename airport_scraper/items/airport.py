import scrapy


class AirportItem(scrapy.Item):
    name = scrapy.Field()
    icao = scrapy.Field()

    arrivals = scrapy.Field()
    departures = scrapy.Field()

    weather_temp = scrapy.Field()
    weather_wind_speed_kmh = scrapy.Field()
    weather_wind_direction_text = scrapy.Field()
    weather_wind_direction_angle = scrapy.Field()

from urllib.parse import urlencode
import json

import scrapy

from airport_scraper.items.airport import AirportItem


class AirportScraper(scrapy.spiders.CrawlSpider):
    name = "www.flightradar24.com"
    allowed_domains = ['flightradar24.com']

    start_urls = []
    base_url = 'https://api.flightradar24.com/common/v1/airport.json'

    def __init__(self, airports=None, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for icao in airports or []:
            self.start_urls.append('{}?{}'.format(self.base_url, urlencode({'code': icao})))

    results = {}

    def parse_start_url(self, response):
        return self.parse(response)

    def parse(self, response):
        data = json.loads(response.text)['result']['response']['airport']['pluginData']

        name = data['details']['name']
        icao = data['details']['code']['icao']

        if icao not in self.results:
            self.results[icao] = AirportItem()
        item = self.results[icao]

        item['name'] = name
        item['icao'] = icao

        weather = data['weather']
        item['weather_temp'] = weather['temp']['celsius']
        item['weather_wind_speed_kmh'] = weather['wind']['speed']['kmh']
        item['weather_wind_direction_text'] = weather['wind']['direction']['text']
        item['weather_wind_direction_angle'] = weather['wind']['direction']['degree']

        return self.parse_flights(response)

    def parse_flights(self, response):
        data = json.loads(response.text)['result']['response']['airport']['pluginData']

        icao = data['details']['code']['icao']
        item = self.results[icao]

        arrivals = data['schedule']['arrivals']
        departures = data['schedule']['departures']

        if 'arrivals' not in item:
            item['arrivals'] = []

        if 'departures' not in item:
            item['departures'] = []

        item['arrivals'] += [x['flight'] for x in arrivals['data']]
        item['departures'] += [x['flight'] for x in departures['data']]

        current_page = max(arrivals['page']['current'], departures['page']['current'])
        total_pages = max(arrivals['page']['total'], departures['page']['total'])

        next_page = current_page + 1

        if next_page <= total_pages:
            return scrapy.Request('{}?{}'.format(self.base_url, urlencode({'code': icao, 'page': next_page})))

        return item

import argparse
from datetime import datetime

from scrapy.crawler import CrawlerProcess
from tabulate import tabulate

from airport_scraper.spiders.airports import AirportScraper
from utility import safe_get


def scrap(airports_icao, settings=None):
    scraper = AirportScraper(airports=airports_icao, )

    process = CrawlerProcess(settings=settings)
    process.crawl(scraper)
    process.start()

    return dict(scraper.results)


def print_results(results):
    for icao, data in sorted(results.items(), key=lambda x: x[1]["weather_wind_speed_kmh"]):
        title = f'{icao} | {data["name"]}'
        weather = \
            f'Temperature {data["weather_temp"]} °C | ' \
            f'Wind {data["weather_wind_direction_text"].lower()} (angle {data["weather_wind_direction_angle"]}°) ' \
            f'with speed {data["weather_wind_speed_kmh"]} km/h'

        arrivals_data = [
            [
                safe_get(x, 'time', 'scheduled', 'arrival'), safe_get(x, 'time', 'real', 'arrival'),
                safe_get(x, 'identification', 'number', 'default'),
                safe_get(x, 'airport', 'origin', 'name'), safe_get(x, 'airline', 'name'),
                safe_get(x, 'aircraft', 'model', 'text') or safe_get(x, 'aircraft', 'model', 'code'),
                safe_get(x, 'status', 'text'),
            ]
            for x in data['arrivals']
        ]

        departure_data = [
            [
                safe_get(x, 'time', 'scheduled', 'departure'), safe_get(x, 'time', 'real', 'departure'),
                safe_get(x, 'identification', 'number', 'default'),
                safe_get(x, 'airport', 'destination', 'name'), safe_get(x, 'airline', 'name'),
                safe_get(x, 'aircraft', 'model', 'text') or safe_get(x, 'aircraft', 'model', 'code'),
                safe_get(x, 'status', 'text'),
            ]
            for x in data['departures']
        ]

        # replace timestamps to datetime objects
        for table in [arrivals_data, departure_data]:
            for row in table:
                for i in [0, 1]:  # indices of columns with values that need to be converted to datetime
                    if row[i]:
                        row[i] = datetime.fromtimestamp(row[i])

        arrivals_table = tabulate(
            arrivals_data,
            headers=['scheduled datetime', 'real datetime', 'flight id', 'from', 'airline', 'aircraft', 'status']
        )
        departure_table = tabulate(
            departure_data,
            headers=['scheduled datetime', 'real datetime', 'flight id', 'to', 'airline', 'aircraft', 'status']
        )

        print(
            f'\n\n\n',
            f'{title}',
            f'{weather}',
            f'\n',
            f'Arrivals:',
            f'{arrivals_table}',
            f'\n',
            f'Departures:',
            f'{departure_table}',

            sep='\n', end=''
        )

    print()


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '-a', '--airports', type=str, nargs='+',
        help='a list of airports ICAO or IATA'
    )
    parser.add_argument(
        '--enable-logs', action='store_true'
    )

    args = parser.parse_args()

    airports_icao = args.airports
    settings = {
        "LOG_ENABLED": args.enable_logs
    }

    results = scrap(airports_icao, settings)

    print_results(results)


if __name__ == "__main__":
    main()

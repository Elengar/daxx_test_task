### Task description
Write a command line app which shows the list of airports with current weather conditions and arrivals/departures list, ordered by the wind speed at the airport.  
The parameter is the list of airports. The details of realisation are up to you, you can use ICAO codes as parameters and Flightradar24 as a data source.  

### Preinstall (if you don't have installed python3.6)
sudo apt-get install python3.6 python3.6-dev  

### Install 
./init.sh  

### Run  
./run.sh {arguments}  

##### Examples  
./run.sh -a kbp iev  
./run.sh --airports kbp iev  
./run.sh --airports kbp iev egll  
./run.sh --airports kbp iev egll --enable-logs

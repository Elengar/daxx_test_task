DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

VENV_DIRECTORY_NAME="venv"
VENV_DIRECTORY_PATH="$DIR/$VENV_DIRECTORY_NAME"

# Activate venv
source "$VENV_DIRECTORY_PATH/bin/activate"


python main.py "$@"
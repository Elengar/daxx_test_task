
def safe_get(dict_, *keys):
    for key in keys:
        try:
            dict_ = dict_[key]
        except (KeyError, TypeError):
            return None
    return dict_
